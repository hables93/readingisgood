# Readingisgood Service

[Swagger UI](http://localhost:8001/swagger-ui.html)

## Libraries
* Spring Boot 2.6.0
* Spring Boot Test
* h2database
* javax.validation 2.0.0.Final
* spring-boot-starter-validation
* projectlombok
* aspectj
* spring-boot-starter-security
* jsonwebtoken
* Swagger API


## Docker
docker build . -t readingisgood:0.0.1

docker run -d --name deneme -p 8001:8001 readingisgood-0.0.1

### Active profile
-Dspring.profiles.active=local

### Authentication 

To get a valid token, you must log in to the service with a valid customer email and password. Response token parameter in response It should be added to "Headers" with Authorization = token.

http://localhost:8001/user

Example Request : 

{
"email" : "test1@test.com",
"password" : "123456"
}

## Hint 
What if it happens if 2 or more users tries to buy one last book at the same time? 
Normally incoming requests at the same time are updated on the database after receiving approval
from the stock control and the stock is updated as -1.

As a solution, with use @Synchronized and @Transactional annotations
I ensured that only one of the transactions was made the others were terminated with the exception
and the database changes if any were rolled back.


## Services
* CustomerService
    * login
    * registerCustomer
    * getCustomer
    * getCustomerList
    * updateCustomer
    * deleteCustomer
    * checkEmail
    * updateCustomerPassword
 
* BookService   
    * insertBook
    * getBook
    * getBookList
    * updateBook
    * deleteBook
    * updateBookStock

* OrderService
   * insertOrder
   * getOrder
   * getOrderList
   * getOrderDetail
   * updateOrder
   * deleteOrder
   * updateOrderStatus
   * getCustomerOrders

* StatisticsService
   * getStatistics
   
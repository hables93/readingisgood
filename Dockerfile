FROM openjdk:11-jdk-slim AS  build

ARG SPRING_PROFILES_ACTIVE=local
EXPOSE 8001
COPY pom.xml mvnw ./
COPY .mvn .mvn
RUN ./mvnw dependency:resolve

COPY src src
RUN ./mvnw package

FROM openjdk:11
WORKDIR Readingisgood-api
COPY --from=build target/*.jar Readingisgood-api.jar
ENTRYPOINT ["java", "-jar","-Dspring.profiles.active=local", "Readingisgood-api.jar"]


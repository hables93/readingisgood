package com.getir.service.readingisgood.controller;

import com.getir.service.readingisgood.aop.LogRequestResponse;
import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(value = "Customer Service")
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @ApiOperation(
            value = "Provides create new customer.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Customer Object. </li>" +
                    "	<li>Return Customer Object with id.</li>" +
                    "</ul>")
    @PostMapping("/")
    @LogRequestResponse
    public Customer registerCustomer(@RequestBody @Valid Customer request){
        return customerService.registerCustomer(request);
    }

    @ApiOperation(
            value = "Provides get costumer with costumer id.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects int customer id.</li>" +
                    "	<li>Return Costumer Object.</li>" +
                    "</ul>")
    @GetMapping("/{id}")
    @LogRequestResponse
    public Customer getCustomer(@PathVariable int id){
        return customerService.getCustomer(id);
    }

    @ApiOperation(
            value = "Provides get costumer list.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects parameterless.</li>" +
                    "	<li>Return all Costumers</li>" +
                    "</ul>")
    @GetMapping("/")
    @LogRequestResponse
    public List<Customer> getCustomerList(){
        return customerService.getCustomerList();
    }

    @ApiOperation(
            value = "Provides update customer.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Costumer Object.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @PutMapping("/")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCustomer(@RequestBody Customer customer){
        customerService.updateCustomer(customer);
    }

    @PutMapping("/{id}")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCustomerPassword(@RequestBody Customer customer, @PathVariable int id){
        customer.setId(id);
        customerService.updateCustomerPassword(customer);
    }

    @ApiOperation(
            value = "Provides delete customer.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Customer id.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @DeleteMapping("/{id}")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable int id){
        customerService.deleteCustomer(id);
    }

}

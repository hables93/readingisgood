package com.getir.service.readingisgood.controller;

import com.getir.service.readingisgood.aop.LogRequestResponse;
import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.model.OrderModel;
import com.getir.service.readingisgood.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "Order Service")
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation(
            value = "Provides create new order.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Order Object. </li>" +
                    "	<li>Return Order Object with id.</li>" +
                    "</ul>")
    @LogRequestResponse
    @PostMapping("/")
    public Order insertOrder(@RequestBody Order request){
        return orderService.insertOrder(request);
    }

    @ApiOperation(
            value = "Provides get order with order id.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects int order id.</li>" +
                    "	<li>Return Order Object</li>" +
                    "</ul>")
    @LogRequestResponse
    @GetMapping("/{id}")
    public Order getOrder(@PathVariable int id){
        return orderService.getOrder(id);
    }

    @ApiOperation(
            value = "Provides get order list.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects parameterless.</li>" +
                    "	<li>Return all Orders</li>" +
                    "</ul>")
    @LogRequestResponse
    @GetMapping("/")
    public List<Order> getOrderList(){
        return orderService.getOrderList();
    }

    @ApiOperation(
            value = "Provides get order detail.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects order id.</li>" +
                    "	<li>Return OrderModel Object.</li>" +
                    "</ul>")
    @LogRequestResponse
    @GetMapping("/detail/{id}")
    public OrderModel getOrderDetail(@PathVariable int id){
        return orderService.getOrderDetail(id);
    }

    @ApiOperation(
            value = "Provides get customer's orders.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects customer id and pageNo.</li>" +
                    "	<li>pageNo set offset for paging.</li>" +
                    "	<li>Return list of OrderModel Object.</li>" +
                    "</ul>")
    @LogRequestResponse
    @GetMapping("/customer/{customerId}/{pageNo}")
    public List<OrderModel> getCustomerOrders(@PathVariable int customerId, @PathVariable int pageNo){
        return orderService.getCustomerOrders(customerId,pageNo);
    }

    @ApiOperation(
            value = "Provides update order.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Order Object.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @LogRequestResponse
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrder(@RequestBody Order request, @PathVariable int id){
        request.setId(id);
        orderService.updateOrder(request);
    }

    @ApiOperation(
            value = "Provides delete order.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Order id.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @LogRequestResponse
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable int id){
        orderService.deleteOrder(id);
    }

    @LogRequestResponse
    @PutMapping("/status/")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateOrderStatus(@RequestBody Order request){
        orderService.updateOrderStatus(request);
    }

}

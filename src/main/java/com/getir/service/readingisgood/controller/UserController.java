package com.getir.service.readingisgood.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.getir.service.readingisgood.aop.LogRequestResponse;
import com.getir.service.readingisgood.exceptions.models.BadRequestException;
import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.model.User;
import com.getir.service.readingisgood.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Api(value = "User Service")
@RestController
public class UserController {

	@Autowired
	private CustomerService customerService;

	@ApiOperation(
			value = "Provides get token for authentication.",
			notes = "<ul style=\"font-size:120%\">" +
					"	<li>Expects Customer Object. </li>" +
					"	<li>Return User Object with token.</li>" +
					"</ul>")
	@LogRequestResponse
	@PostMapping("user")
	public User login(@RequestBody Customer customer) {
		if (customerService.login(customer)) {
			String token = getJWTToken(customer.getEmail()+customer.getPassword());

			User user = new User();
			user.setUser(customer.getEmail());
			user.setToken(token);

			return user;
		} else {
			throw new BadRequestException("Failed login!");
		}
	}

	private String getJWTToken(String username) {
		String secretKey = "mySecretKey";
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		return "Bearer " + token;
	}
}
package com.getir.service.readingisgood.controller;

import com.getir.service.readingisgood.aop.LogRequestResponse;
import com.getir.service.readingisgood.model.Statistic;
import com.getir.service.readingisgood.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "Statistic Service")
@RequestMapping("/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @ApiOperation(
            value = "Provides gets monthly statistics.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Return list of Statistic Object</li>" +
                    "</ul>")
    @LogRequestResponse
    @GetMapping("/")
    public List<Statistic> getStatistics(){
        return statisticsService.getStatistics();
    }
}

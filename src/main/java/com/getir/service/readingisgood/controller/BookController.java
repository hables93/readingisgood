package com.getir.service.readingisgood.controller;

import com.getir.service.readingisgood.aop.LogRequestResponse;
import com.getir.service.readingisgood.model.Book;
import com.getir.service.readingisgood.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "Book Service")
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @ApiOperation(
            value = "Provides create new book.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Book Object. </li>" +
                    "	<li>Return Book Object with id.</li>" +
                    "</ul>")
    @PostMapping("/")
    @LogRequestResponse
    public Book insertBook(@RequestBody Book request){
        return bookService.insertBook(request);
    }

    @ApiOperation(
            value = "Provides get book with book id.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects int book id.</li>" +
                    "	<li>Return Book Object</li>" +
                    "</ul>")
    @GetMapping("/{id}")
    @LogRequestResponse
    public Book getBook(@PathVariable int id){
        return bookService.getBook(id);
    }

    @ApiOperation(
            value = "Provides get book list.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects parameterless.</li>" +
                    "	<li>Return all Books</li>" +
                    "</ul>")
    @GetMapping("/")
    @LogRequestResponse
    public List<Book> getBookList(){
        return bookService.getBookList();
    }

    @ApiOperation(
            value = "Provides update book.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Book Object.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @PutMapping("/")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBook(@RequestBody Book book){
        bookService.updateBook(book);
    }

    @ApiOperation(
            value = "Provides delete book.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects Book id.</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @DeleteMapping("/{id}")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBook(@PathVariable int id){
        bookService.deleteBook(id);
    }

    @ApiOperation(
            value = "Provides update stock.",
            notes = "<ul style=\"font-size:120%\">" +
                    "	<li>Expects number of piece and book id</li>" +
                    "	<li>Return 204 No Content</li>" +
                    "</ul>")
    @PutMapping("/{id}")
    @LogRequestResponse
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBookStock(@RequestBody int piece ,@PathVariable int id){
        bookService.updateBookStock(piece, id);
    }

}

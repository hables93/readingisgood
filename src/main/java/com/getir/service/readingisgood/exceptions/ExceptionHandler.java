package com.getir.service.readingisgood.exceptions;

import com.getir.service.readingisgood.exceptions.models.ApiException;
import com.getir.service.readingisgood.exceptions.models.AuthException;
import com.getir.service.readingisgood.exceptions.models.BadRequestException;
import com.getir.service.readingisgood.exceptions.models.NotFoundException;
import com.getir.service.readingisgood.exceptions.models.enums.Errors;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;


@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        final Error error = new Error(HttpStatus.BAD_REQUEST, "Malformed JSON request", ex);
        return buildResponseEntity(error);
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final List<String> errors = new ArrayList<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        final Error error = new Error(HttpStatus.BAD_REQUEST);
        error.setMessage(ex.getMessage());
        error.setDetail(errors.toString());
        error.setType("FieldError");
        error.setCode(Errors.BAD_REQUEST.getCode());
        return buildResponseEntity(error);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex) {
        final Error error = new Error(HttpStatus.NOT_FOUND);
        error.setMessage(ex.getMessage());
        error.setDetail(ex.getMessage());
        error.setType(ex.getType());
        error.setCode(ex.getCode());
        return buildResponseEntity(error);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(ApiException.class)
    protected ResponseEntity<Object> apiExceptionHandler(ApiException ex) {
        final Error error = new Error(ex.getStatus());
        error.setTraceId(ex.getTraceId());
        error.setMessage(ex.getMessage());
        error.setDetail(ex.getDetail());
        error.setType(ex.getType());
        error.setCode(ex.getCode());
        return buildResponseEntity(error);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException ex) {
        final Error error = new Error(HttpStatus.BAD_REQUEST);
        error.setMessage(ex.getMessage());
        error.setDetail(ex.getMessage());
        if (ex.getCause() != null) {
            error.setDetail(ex.getCause().getMessage());
        }
        error.setType(ex.getType());
        error.setCode(ex.getCode());
        return buildResponseEntity(error);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(AuthException.class)
    protected ResponseEntity<Object> handleAuthException(AuthException ex) {
        final Error error = new Error(HttpStatus.UNAUTHORIZED);
        error.setMessage(ex.getMessage());
        error.setDetail(ex.getMessage());
        if (ex.getCause() != null) {
            error.setDetail(ex.getCause().getMessage());
        }
        error.setType(ex.getType());
        error.setCode(ex.getCode());
        return buildResponseEntity(error);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception ex) {
        Error error = new Error(HttpStatus.INTERNAL_SERVER_ERROR);
        error.setMessage(ex.getMessage());
        error.setDetail(ex.getMessage());
        return buildResponseEntity(error);
    }

    private ResponseEntity<Object> buildResponseEntity(Error error) {
        return new ResponseEntity<>(error, error.getStatus());
    }


}


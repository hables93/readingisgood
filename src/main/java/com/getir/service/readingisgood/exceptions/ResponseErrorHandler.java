package com.getir.service.readingisgood.exceptions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.exceptions.models.ApiException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.ClientHttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ResponseErrorHandler implements org.springframework.web.client.ResponseErrorHandler {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    @Value("${ditravo.service.name:#{null}}")
    private String serviceName;

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getStatusCode().isError();
    }

    @Override
    public void handleError(ClientHttpResponse response)
            throws IOException {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))) {
            final String httpBodyResponse = reader.lines().collect(Collectors.joining(""));
            final Error error = objectMapper.readValue(httpBodyResponse, Error.class);
            throw ApiException.of(error, serviceName);
        }
    }
}

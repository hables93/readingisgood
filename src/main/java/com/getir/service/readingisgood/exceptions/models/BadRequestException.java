package com.getir.service.readingisgood.exceptions.models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.exceptions.models.enums.Errors;
import lombok.SneakyThrows;

public class BadRequestException extends Exception {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public BadRequestException() {
        this.setError(Errors.BAD_REQUEST);
    }

    public BadRequestException(final String message) {
        this();
        this.setMessage(message);
    }

    public BadRequestException(Class clazz, Object value) {
        this.setMessage(String.format("Bad request error %s with value %s not found", clazz.getCanonicalName(), value));
    }

    public static BadRequestException ofProcess(String message){
        final BadRequestException badRequestException = new BadRequestException();
        badRequestException.setError(Errors.BAD_REQUEST_PROCESS);
        badRequestException.setMessage(message);
        return badRequestException;
    }
    @SneakyThrows
    public static BadRequestException ofProcessObject(String message, Object value){
        final BadRequestException badRequestException = new BadRequestException();
        badRequestException.setError(Errors.BAD_REQUEST_PROCESS);
        badRequestException.setMessage(message);
        badRequestException.initCause(new BadRequestException(objectMapper.writeValueAsString(value)));
        return badRequestException;
    }

    public static BadRequestException ofException(java.lang.Exception ex){
        final BadRequestException badRequestException = new BadRequestException();
        badRequestException.setError(Errors.BAD_REQUEST_PROCESS);
        badRequestException.setMessage(ex.getMessage());
        badRequestException.initCause(ex);
        return badRequestException;
    }

    public static BadRequestException of(final Errors errors) {
        final BadRequestException badRequestException = new BadRequestException();
        badRequestException.setError(errors);
        return badRequestException;
    }
}

package com.getir.service.readingisgood.exceptions.models.enums;

import lombok.Getter;

@Getter
public enum Errors {
    // 600-630 generic errors
    BAD_REQUEST(600, "Parameters or request is not valid"),
    AUTH_FAILED(601, "Credentials provided are not valid"),
    NOT_AUTHORIZED(603, "Not authorized to access"),
    ENTITY_MISSING(604, "Entity is not found"),
    INVALID_METHOD(605, "Request method is not valid"),
    CONFLICT(609, "Data conflict"),
    TOO_MANY_REQUESTS(629, "Request limit is reached"),
    BAD_REQUEST_PROCESS(690, "Request can not be processed"),

    // 630-640 Auth-Token errors
    AUTH_INVALID(631, "Credentials are not valid"),
    TOKEN_INVALID(632, "Token is not valid or expired"),
    PASSWORD_NOT_STRONG(632, "Password is not strong"),

    // 640-660 Payment errors
    PAYMENT_FAILED(640,"Payment failed"),
    PAYMENT_3D_FAILED (641,"3D verification failed"),
    PAYMENT_UNREACHABLE (642,"Payment endpoint is not reachable"),
    PAYMENT_LIMIT_FAILED(643,"Credit limit is insufficient"),

    // 661-665 Coupon errors
    COUPON_BALANCE_INSUFFICIENT(661,"Coupon balance is insufficient"),
    COUPON_IS_NOT_VALID(662,"Coupon is not valid"),

    // 680-700 Email errors
    EMAIL_SEND_FAILED(680, "Failed to send the email"),

    // 701-710 User errors
    INVALID_CREDENTIALS(701, "User mail or password does not match");



    private final int code;
    private final String message;

    Errors(int code, String message){
        this.code = code;
        this.message = message;
    }
    
}
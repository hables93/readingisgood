package com.getir.service.readingisgood.exceptions.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getir.service.readingisgood.exceptions.Error;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
public class ApiException extends RuntimeException {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private Date timestamp;
    private HttpStatus status;
    private String traceId;
    private String type;
    private int code;
    private String message;
    private String detail;
    private String service;

    public static ApiException of(final Error error, final String service) {
        final ApiException apiException = new ApiException();
        apiException.setTimestamp(error.getTimestamp());
        apiException.setTraceId(error.getTraceId());
        apiException.setStatus(error.getStatus());
        apiException.setType(error.getType());
        apiException.setCode(error.getCode());
        apiException.setMessage(error.getMessage());
        apiException.setDetail(error.getDetail());
        apiException.setService(service);
        return apiException;
    }
}

package com.getir.service.readingisgood.exceptions.models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.exceptions.models.enums.Errors;
import lombok.SneakyThrows;

public class NotFoundException extends Exception {

    private final static ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    public static NotFoundException ofProcessObject(String message, Object value) {
        final NotFoundException notFoundException = new NotFoundException();
        notFoundException.setError(Errors.ENTITY_MISSING);
        notFoundException.setMessage(message);
        notFoundException.initCause(new NotFoundException(objectMapper.writeValueAsString(value)));
        return notFoundException;
    }


    public NotFoundException() {
        setError(Errors.ENTITY_MISSING);
    }

    public NotFoundException(Class clazz, Object value) {
        this();
        this.setMessage(String.format("Entity %s with value {%s} not found", clazz.getSimpleName(), value));
    }

    public NotFoundException(final String message) {
        this();
        this.setMessage(message);
    }

    public static NotFoundException of(final String message) {
        return new NotFoundException(message);
    }

}

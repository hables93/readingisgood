package com.getir.service.readingisgood.exceptions.models;

import com.getir.service.readingisgood.exceptions.models.enums.Errors;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public abstract class Exception extends RuntimeException {
    private int code;
    private String message;
    private String type;

    public void setError(final Errors error){
        this.message = error.getMessage();
        this.code = error.getCode();
        this.type = error.name();
    }

    public Exception(final Errors error) {
        this.message = error.getMessage();
        this.code = error.getCode();
        this.type = error.name();
    }
}
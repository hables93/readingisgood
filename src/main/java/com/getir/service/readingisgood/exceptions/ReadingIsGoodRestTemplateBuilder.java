package com.getir.service.readingisgood.exceptions;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class ReadingIsGoodRestTemplateBuilder {

    @Primary
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .errorHandler(new ResponseErrorHandler()).build();
    }
}

package com.getir.service.readingisgood.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getir.service.readingisgood.exceptions.models.enums.Errors;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
@Slf4j
public class Error {
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    private Date timestamp;
    private String traceId;
    private HttpStatus status;
    private String type;
    private int code;
    private String message;
    private String detail;

    private Error() {
        timestamp = new Date();
        traceId = MDC.get("X-B3-TraceId");
    }

    Error(HttpStatus status) {
        this();
        this.status = status;
    }

    Error(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.message = "Unresolved error";
        this.detail = ex.getLocalizedMessage();
    }

    Error(HttpStatus status, Errors error) {
        this();
        this.status = status;
        this.code = error.getCode();
    }


    Error(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status;
        this.message = message;
        this.detail = ex.getLocalizedMessage();
    }
}

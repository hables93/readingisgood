package com.getir.service.readingisgood.exceptions.models;

import com.getir.service.readingisgood.exceptions.models.enums.Errors;

public class AuthException extends Exception {

    public AuthException() {
        this.setError(Errors.AUTH_FAILED);
    }

    public AuthException(Errors errors) {
        this.setError(errors);
    }

    public AuthException(final String message) {
        this();
        this.setMessage(message);
    }


    public static AuthException of(final Errors errors) {
        final AuthException authException = new AuthException();
        authException.setError(errors);
        return authException;
    }

    public static AuthException of(final Errors errors, final String message) {
        final AuthException authException = new AuthException();
        authException.setError(errors);
        authException.setMessage(message);
        return authException;
    }

}

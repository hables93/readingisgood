package com.getir.service.readingisgood.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

@Aspect
@Component
@Log4j2
public class LogRequestResponseAspect {
	@Autowired
	private ObjectMapper objectMapper;

	@Around("@annotation(LogRequestResponse)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {

		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = null;

		try {
			request = attributes.getRequest();
		}
		catch (ArrayIndexOutOfBoundsException e) {
			//Service can have no request
			log.info("Service has no request : {}",e.getMessage() );
		}

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		MDC.put("uri", request.getRequestURI());
		MDC.put("url_", String.valueOf(request.getRequestURL()));
		MDC.put("query-string", request.getQueryString());
		MDC.put("qualified-name",joinPoint
				.getSignature()
				.getDeclaringTypeName()
				.concat(".") + joinPoint.getSignature().getName().concat("()"));
		MDC.put("method-name",signature.getMethod().getName());
		MDC.put("log-type", "REQUEST");

		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));

		log.info("{}", objectMapper.writeValueAsString(joinPoint.getArgs()));

		Object response = joinPoint.proceed();
		MDC.put("log-type", "RESPONSE");
		log.info("{}", objectMapper.writeValueAsString(response));

		return response;
	}
}

package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Statistic;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class StatisticsDaoHelper {

    public RowMapper<Statistic> statisticRowMapper = (rs, rowNum) -> {
        Statistic entity = new Statistic();

        entity.setMonth(rs.getString("month"));
        entity.setTotalOrderCount(rs.getInt("total_order_count"));
        entity.setTotalBookCount(rs.getInt("total_book_count"));
        entity.setTotalPrice(rs.getDouble("total_price"));

        return entity;
    };

}

package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatisticsDao {

    @Autowired
    private StatisticsDaoHelper statisticsDaoHelper;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<Statistic> getStatistics() {
        return namedParameterJdbcTemplate.query(StatisticsSqlStatements.GET_STATISTICS_SQL,
                statisticsDaoHelper.statisticRowMapper);
    }
}

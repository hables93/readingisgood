package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerDaoHelper {

    public RowMapper<Customer> customerRowMapper = (rs, rowNum) -> {
        Customer entity = new Customer();

        entity.setId(rs.getInt("id"));
        entity.setName(rs.getString("name"));
        entity.setSurname(rs.getString("surname"));
        entity.setEmail(rs.getString("email"));
        entity.setPassword(rs.getString("password"));

        return entity;
    };
}

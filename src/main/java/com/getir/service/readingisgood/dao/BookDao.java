package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BookDao {

    @Autowired
    private BookDaoHelper bookDaoHelper;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public int insertBook(Book book){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource =
                new MapSqlParameterSource()
                        .addValue("name", book.getName())
                        .addValue("stock", book.getStock())
                        .addValue("price", book.getPrice());

        namedParameterJdbcTemplate.update(BookDaoSqlStatements.INSERT_BOOK_SQL, parameterSource, keyHolder);
        return (int) keyHolder.getKeyList().get(0).get("id");
    }

    public Book getBook(int id){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(BookDaoSqlStatements.GET_BOOK_BY_ID_SQL, sqlParameterSource,
                bookDaoHelper.bookRowMapper));
    }

    public List<Book> getBookList(){
        return namedParameterJdbcTemplate.query(BookDaoSqlStatements.GET_BOOK_LIST_SQL, bookDaoHelper.bookRowMapper);
    }

    public boolean updateBook(Book book){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", book.getId())
                .addValue("name", book.getName())
                .addValue("stock", book.getStock())
                .addValue("price", book.getPrice());

        return namedParameterJdbcTemplate.update(BookDaoSqlStatements.UPDATE_BOOK_SQL, sqlParameterSource) == 1;
    }

    public boolean deleteBook(int id){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        return namedParameterJdbcTemplate.update(BookDaoSqlStatements.DELETE_BOOK_SQL, sqlParameterSource) == 1;
    }


    public boolean updateBookStock(int piece, int id){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("piece", piece);

        return namedParameterJdbcTemplate.update(BookDaoSqlStatements.UPDATE_BOOK_STOCK_SQL, sqlParameterSource) == 1;
    }

}

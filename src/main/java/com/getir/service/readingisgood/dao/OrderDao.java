package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.model.OrderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderDao {

    @Autowired
    private OrderDaoHelper orderDaoHelper;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public int insertOrder(Order order) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource =
                new MapSqlParameterSource()
                        .addValue("bookId", order.getBookId())
                        .addValue("customerId", order.getCustomerId())
                        .addValue("orderDate", order.getOrderDate())
                        .addValue("piece", order.getOrderDate())
                        .addValue("price", order.getPiece())
                        .addValue("statusId", 1); // Preparing

        namedParameterJdbcTemplate.update(OrderSqlStatements.INSERT_ORDER_SQL, parameterSource, keyHolder);
        return (int) keyHolder.getKeyList().get(0).get("id");
    }

    public Order getOrder(int id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(OrderSqlStatements.GET_ORDER_BY_ID_SQL, sqlParameterSource,
                orderDaoHelper.orderRowMapper));
    }

    public OrderModel getOrderDetail(int id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(OrderSqlStatements.GET_ORDER_DETAIL_BY_ID, sqlParameterSource,
                orderDaoHelper.orderModelRowMapper));
    }

    public List<OrderModel> getCustomerOrders(int customerId, int pageNo) {
        int offset = (5 * (pageNo-1));
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("customerId", customerId)
                .addValue("offset", offset);
        return namedParameterJdbcTemplate.query(OrderSqlStatements.GET_CUSTOMER_ORDERS_SQL,sqlParameterSource, orderDaoHelper.orderModelRowMapper);
    }

    public List<Order> getOrderList() {
        return namedParameterJdbcTemplate.query(OrderSqlStatements.GET_ORDER_LIST_SQL, orderDaoHelper.orderRowMapper);
    }

    public boolean updateOrder(Order order) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", order.getId())
                .addValue("bookId", order.getBookId())
                .addValue("customerId", order.getCustomerId())
                .addValue("piece", order.getPiece())
                .addValue("price", order.getPrice())
                .addValue("orderDate", order.getOrderDate());

        return namedParameterJdbcTemplate.update(OrderSqlStatements.UPDATE_ORDER_SQL, sqlParameterSource) == 1;
    }

    public boolean deleteOrder(int id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        return namedParameterJdbcTemplate.update(OrderSqlStatements.DELETE_ORDER_SQL, sqlParameterSource) == 1;
    }

    public boolean updateOrderStatus(Order order) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", order.getId())
                .addValue("statusId", order.getStatusId());

        return namedParameterJdbcTemplate.update(OrderSqlStatements.UPDATE_ORDER_STATUS_SQL, sqlParameterSource) == 1;
    }

}
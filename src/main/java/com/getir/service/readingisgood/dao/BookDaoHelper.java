package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class BookDaoHelper {

    public RowMapper<Book> bookRowMapper = (rs, rowNum) -> {
        Book entity = new Book();

        entity.setId(rs.getInt("id"));
        entity.setName(rs.getString("name"));
        entity.setStock(rs.getInt("stock"));
        entity.setPrice(rs.getDouble("price"));

        return entity;
    };
}

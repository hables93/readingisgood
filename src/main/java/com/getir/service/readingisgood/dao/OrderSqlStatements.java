package com.getir.service.readingisgood.dao;

public class OrderSqlStatements {

    public static String GET_ORDER_BY_ID_SQL = "SELECT * from \"order\" where id = :id;";

    public static String GET_ORDER_DETAIL_BY_ID = "SELECT o.id as order_id, o.book_id as book_id, o.customer_id as customer_id, piece, o.price as total_price, order_date, b.name as book_name, stock, b.price as book_price, c.name as customer_name, c.surname as customer_surname, email, os.name as status\n" +
            " from \"order\" o, book b, customer c , order_status os where o.id = :id AND o.book_id = b.id  AND o.customer_id = c.id AND o.status_id = os.id";

    public static String GET_ORDER_LIST_SQL = "SELECT * FROM \"order\" order by order_date asc";

    public static String GET_CUSTOMER_ORDERS_SQL = "SELECT o.id as order_id, o.book_id as book_id, o.customer_id as customer_id, piece," +
            " o.price as total_price, order_date, b.name as book_name, stock, b.price as book_price, c.name as customer_name," +
            " c.surname as customer_surname, email, os.name as status" +
            "  from \"order\" o, book b, customer c , order_status os " +
            "where  c.id = :customerId AND o.customer_id = c.id AND o.book_id = b.id AND o.status_id = os.id" +
            " limit 5 offset :offset";

    public static String INSERT_ORDER_SQL = "INSERT INTO \"order\"(book_id, customer_id, order_date,piece,price,status_id) VALUES " +
            "(:bookId,:customerId,now(),:piece,:price,:statusId);";

    public static String UPDATE_ORDER_SQL = "UPDATE \"order\" set book_id = COALESCE(:bookId,book_id), " +
            "customer_id = COALESCE(:customerId,customer_id), " +
            "piece = COALESCE(:piece,piece), " +
            "price = COALESCE(:price,price), " +
            "order_date = COALESCE(:orderDate,order_date) where id = :id;";

    public static String DELETE_ORDER_SQL = "DELETE from \"order\" where id = :id;";

    public static String UPDATE_ORDER_STATUS_SQL = "UPDATE \"order\" set status_id = COALESCE(:statusId,status_id) where id = :id;";

}

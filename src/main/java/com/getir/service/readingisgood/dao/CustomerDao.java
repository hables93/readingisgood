package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerDao {

    @Autowired
    private CustomerDaoHelper customerDaoHelper;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public int insertCustomer(Customer customer){
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource =
                new MapSqlParameterSource()
                        .addValue("name", customer.getName())
                        .addValue("surname", customer.getSurname())
                        .addValue("email", customer.getEmail())
                        .addValue("password", customer.getPassword());

        namedParameterJdbcTemplate.update(CustomerSqlStatements.INSERT_CUSTOMER_SQL, parameterSource, keyHolder);
        return (int) keyHolder.getKeyList().get(0).get("id");
    }

    public Customer getCustomer(int id){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(CustomerSqlStatements.GET_CUSTOMER_BY_ID_SQL, sqlParameterSource,
                customerDaoHelper.customerRowMapper));
    }

    public Customer getCustomerByEmail(String email){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("email", email);

        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(CustomerSqlStatements.GET_CUSTOMER_BY_EMAIL_SQL, sqlParameterSource,
                customerDaoHelper.customerRowMapper));
    }

    public boolean getCustomerLogin(Customer customer){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("email", customer.getEmail())
                .addValue("password", customer.getPassword());

        return DataAccessUtils.singleResult(namedParameterJdbcTemplate.query(CustomerSqlStatements.CHECK_CUSTOMER_LOGIN_SQL, sqlParameterSource,
                customerDaoHelper.customerRowMapper)) != null;
    }

    public List<Customer> getCustomerList(){
        return namedParameterJdbcTemplate.query(CustomerSqlStatements.GET_CUSTOMER_LIST_SQL, customerDaoHelper.customerRowMapper);
    }

    public boolean updateCustomer(Customer customer){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", customer.getId())
                .addValue("name", customer.getName())
                .addValue("surname", customer.getSurname())
                .addValue("email", customer.getEmail());

        return namedParameterJdbcTemplate.update(CustomerSqlStatements.UPDATE_CUSTOMER_SQL, sqlParameterSource) == 1;
    }

    public boolean deleteCustomer(int id){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);
        return namedParameterJdbcTemplate.update(CustomerSqlStatements.DELETE_CUSTOMER_SQL, sqlParameterSource) == 1;
    }

    public boolean updateCustomerPassword(Customer customer){
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id", customer.getId())
                .addValue("password", customer.getPassword());

        return namedParameterJdbcTemplate.update(CustomerSqlStatements.UPDATE_CUSTOMER_PASSWORD_SQL, sqlParameterSource) == 1;
    }

}

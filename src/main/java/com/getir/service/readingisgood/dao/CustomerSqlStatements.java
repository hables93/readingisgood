package com.getir.service.readingisgood.dao;

public class CustomerSqlStatements {

    public static String GET_CUSTOMER_BY_ID_SQL = "SELECT * from customer where id = :id;";

    public static String GET_CUSTOMER_BY_EMAIL_SQL = "SELECT * from customer where email = :email;";

    public static String CHECK_CUSTOMER_LOGIN_SQL = "SELECT * from customer where email = :email AND password = :password;";

    public static String GET_CUSTOMER_LIST_SQL = "SELECT * from customer;";

    public static String INSERT_CUSTOMER_SQL = "INSERT INTO customer (name, surname, email, password) values (:name,:surname,:email,:password);";

    public static String UPDATE_CUSTOMER_SQL = "UPDATE customer set name = COALESCE(:name,name), surname = COALESCE(:surname,surname)," +
            " email = COALESCE(:email,email) where id = :id;";

    public static String UPDATE_CUSTOMER_PASSWORD_SQL = "UPDATE customer set password = COALESCE(:password,password) where id = :id;";

    public static String DELETE_CUSTOMER_SQL = "DELETE from customer where id = :id;";
}

package com.getir.service.readingisgood.dao;

public class BookDaoSqlStatements {

    public static String GET_BOOK_BY_ID_SQL = "SELECT * from book where id = :id;";

    public static String GET_BOOK_LIST_SQL = "SELECT * from book;";

    public static String INSERT_BOOK_SQL = "INSERT INTO  book (name, stock, price) values (:name, :stock, :price);";

    public static String UPDATE_BOOK_SQL = "UPDATE book set name = COALESCE(:name,name), stock = COALESCE(:stock,stock)," +
            "price = COALESCE(:price,price) where id= :id;";

    public static String DELETE_BOOK_SQL = "DELETE from book where id = :id;";

    public static String UPDATE_BOOK_STOCK_SQL = "UPDATE book set stock = stock - :piece where id = :id;";
}

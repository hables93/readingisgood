package com.getir.service.readingisgood.dao;

public class StatisticsSqlStatements {

    public static String GET_STATISTICS_SQL =
            "SELECT  Count(to_char(order_date, 'Mon')) as total_order_count,\n" +
                    "                to_char(order_date, 'Mon') as month ,\n" +
                    "                SUM(piece) as total_book_count, \n" +
                    "                SUM(price) as total_price\n" +
                    " FROM \"order\" \n" +
                    "             group by to_char(order_date ,'Mon'), CAST(MONTH(order_date) AS VARCHAR(2)) \n" +
                    "             order by CAST(MONTH(order_date) AS VARCHAR(2))";
}

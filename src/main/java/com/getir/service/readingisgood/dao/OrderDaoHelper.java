package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Book;
import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.model.OrderModel;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class OrderDaoHelper {

    public RowMapper<OrderModel> orderModelRowMapper = (rs, rowNum) -> {
        OrderModel entity = new OrderModel();

        entity.setId(rs.getInt("order_id"));
        entity.setOrderDate(rs.getTimestamp("order_date"));
        entity.setOrderStatus(rs.getString("status"));
        entity.setTotalPrice(rs.getDouble("total_price"));
        entity.setPiece(rs.getInt("piece"));

        Book book = new Book();
        book.setId(rs.getInt("book_id"));
        book.setName(rs.getString("book_name"));
        book.setPrice(rs.getInt("book_price"));
        entity.setBook(book);

        Customer customer = new Customer();
        customer.setId(rs.getInt("customer_id"));
        customer.setName(rs.getString("customer_name"));
        customer.setSurname(rs.getString("customer_surname"));
        customer.setEmail(rs.getString("email"));
        entity.setCustomer(customer);

        return entity;
    };

    public RowMapper<Order> orderRowMapper = (rs, rowNum) -> {
        Order entity = new Order();

        entity.setId(rs.getInt("id"));
        entity.setOrderDate(rs.getTimestamp("order_date"));
        entity.setBookId(rs.getInt("book_id"));
        entity.setCustomerId(rs.getInt("customer_id"));
        entity.setStatusId(rs.getInt("status_id"));
        entity.setPiece(rs.getInt("piece"));
        entity.setPrice(rs.getInt("price"));

        return entity;
    };
}

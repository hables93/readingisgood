package com.getir.service.readingisgood.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(HttpMethod.POST,"/user").permitAll()
				.antMatchers("/").permitAll()
				.antMatchers("/h2-console/**").permitAll()
				.antMatchers("/h2-console/login").permitAll()
				.antMatchers("/swagger-ui.html").permitAll()
				.antMatchers("/swagger-resources/**").permitAll()
				.antMatchers("/v2/api-docs").permitAll()
				.antMatchers("/configuration/ui").permitAll()
				.antMatchers("/swagger-resources/**").permitAll()
				.antMatchers("/configuration/security").permitAll()
				.antMatchers("/swagger-ui.html").permitAll()
				.antMatchers("/swagger-ui/*").permitAll()
				.antMatchers("/webjars/**").permitAll()
				.antMatchers("/v2/**").permitAll()
				.anyRequest().authenticated();

		http.csrf().disable();
		http.headers().frameOptions().disable();
	}
}
package com.getir.service.readingisgood.service;

import com.getir.service.readingisgood.dao.BookDao;
import com.getir.service.readingisgood.exceptions.models.BadRequestException;
import com.getir.service.readingisgood.model.Book;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class BookService {

    @Autowired
    private BookDao bookDao;

    public Book insertBook(Book book) {
        book.setId(bookDao.insertBook(book));
        return book;
    }

    public Book getBook(int id) {
        return bookDao.getBook(id);
    }

    public List<Book> getBookList() {
        return bookDao.getBookList();
    }

    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    public void deleteBook(int id) {
        bookDao.deleteBook(id);
    }

    @Synchronized
    @Transactional
    public boolean updateBookStock(int piece, int id)   {
        if (bookDao.getBook(id).getStock() >= piece ) {
            return bookDao.updateBookStock(piece, id);
        } else
            throw new BadRequestException("Not enough stock!");
    }

}

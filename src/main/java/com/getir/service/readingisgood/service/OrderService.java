package com.getir.service.readingisgood.service;

import com.getir.service.readingisgood.dao.OrderDao;
import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.model.OrderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderService {

    @Autowired
    private OrderDao orderDao;

    public Order insertOrder(Order order) {
        order.setId(orderDao.insertOrder(order));
        return order;
    }

    public Order getOrder(int id){
        return orderDao.getOrder(id);
    }

    public List<Order> getOrderList() {
        return orderDao.getOrderList();
    }


    public OrderModel getOrderDetail(int id) {
        return orderDao.getOrderDetail(id);
    }


    public void updateOrder(Order order) {
        orderDao.updateOrder(order);
    }

    public void deleteOrder(int id) {
        orderDao.deleteOrder(id);
    }

    public void updateOrderStatus(Order order){
        orderDao.updateOrderStatus(order);
    }

    public List<OrderModel> getCustomerOrders(int customerId, int pageNo) {
        return orderDao.getCustomerOrders(customerId, pageNo);
    }
}

package com.getir.service.readingisgood.service;

import com.getir.service.readingisgood.dao.StatisticsDao;
import com.getir.service.readingisgood.model.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StatisticsService {

    @Autowired
    private StatisticsDao statisticsDao;

    public List<Statistic> getStatistics() {
        return statisticsDao.getStatistics();
    }
}

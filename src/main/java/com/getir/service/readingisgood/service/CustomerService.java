package com.getir.service.readingisgood.service;

import com.getir.service.readingisgood.dao.CustomerDao;
import com.getir.service.readingisgood.exceptions.models.BadRequestException;
import com.getir.service.readingisgood.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomerDao customerDao;

    public boolean login(Customer customer){
        if(passwordEncoder.matches(customer.getPassword(),
                customerDao.getCustomerByEmail(customer.getEmail()).getPassword())){
            return true;
        }else {
            throw new BadRequestException("Email or password wrong!");
        }
    }

    public Customer registerCustomer(Customer customer) {
        if (checkEmail(customer.getEmail())){
//          Hash customer password and insert db.
            customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            customer.setId(customerDao.insertCustomer(customer));
            return customer;
        } else {
            throw new BadRequestException("Email already in use.");
        }
    }

    public Customer getCustomer(int id) {
        Customer customer = customerDao.getCustomer(id);
        if (customer != null){
            return customer;
        }
        else {
            throw new BadRequestException("Not found costumer id :"+id);
        }
    }

    public List<Customer> getCustomerList() {
        return customerDao.getCustomerList();
    }

    public void updateCustomer(Customer customer) {
        customerDao.updateCustomer(customer);
    }

    public void deleteCustomer(int id) {
        customerDao.deleteCustomer(id);
    }

    public boolean checkEmail(String email){
        return customerDao.getCustomerByEmail(email) == null;
    }

    public void updateCustomerPassword(Customer customer) {
        customerDao.updateCustomerPassword(customer);
    }
}

package com.getir.service.readingisgood.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private int id;
    private int customerId;
    private int bookId;
    private int piece;
    private double price;
    private int statusId;
    private Timestamp orderDate;
}

package com.getir.service.readingisgood.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {
    private int id;

    @NotNull
    @Size(min = 2 ,message = "Name must be at least two letters.")
    private String name;

    @NotNull
    @Size(min = 2 ,message = "Surname must be at least two letters.")
    private String surname;

    @Email(message = "Enter a valid email address.")
    private String email;

    @NotNull
    @Size(min = 6 ,message = "Password must be at least six characters.")
    private String password;
}

package com.getir.service.readingisgood.model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class OrderModel {
    private int id;
    private Customer customer;
    private Book book;
    private int piece;
    private double totalPrice;
    private Timestamp orderDate;
    private String orderStatus;
}

package com.getir.service.readingisgood.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Statistic {
    private int totalOrderCount;
    private String month;
    private int totalBookCount;
    private double totalPrice;
}


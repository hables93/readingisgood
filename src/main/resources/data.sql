-- Initialize
-- Create tables

-- Create customer table
create table if not exists customer
(
    id serial,
    name VARCHAR,
    surname VARCHAR,
    email VARCHAR,
    password VARCHAR not null
);

create unique index customer_email_uindex
    on customer (email);


-- Create book table
create table if not exists book
(
    id serial
        constraint book_pk
            primary key,
    name VARCHAR,
    stock int,
--     stock int CHECK (stock >= 0),
    price double
);

-- Create order table
create table if not exists "order"
(
    id serial
        constraint order_pk
            primary key,
    book_id int,
    customer_id int,
    status_id int,
    piece int,
    price double,
    order_date timestamp
);

-- Create status_order table
create table if not exists order_status
(
    id serial
        constraint order_status_pk
            primary key,
    name VARCHAR
);
-- Add foreign key order_status.id -> order.status_id
ALTER TABLE "order"
    ADD FOREIGN KEY (status_id)
        REFERENCES order_status(id);


-- Add example data
-- Order Status
INSERT INTO order_status (name) values ('HAZIRLANIYOR');
INSERT INTO order_status (name) values ('KARGODA');
INSERT INTO order_status (name) values ('TESLIM EDILDI');

-- Book
INSERT INTO  book (name, stock, price) values ('book1', 50,10.99);
INSERT INTO  book (name, stock, price) values ('book2', 30,11.99);
INSERT INTO  book (name, stock, price) values ('book3', 44,15.5);
INSERT INTO  book (name, stock, price) values ('book4', 33,4.75);
INSERT INTO  book (name, stock, price) values ('book5', 25,3.5);
INSERT INTO  book (name, stock, price) values ('book6', 7,30);
INSERT INTO  book (name, stock, price) values ('book7', 5,25);
INSERT INTO  book (name, stock, price) values ('book8', 13,15);
INSERT INTO  book (name, stock, price) values ('book9', 28,5);

-- Customer && customer's passwords -> 123456
INSERT INTO customer (name, surname, email, password) values ('alper','karaagacli','test1@test.com','$2a$10$HqCx8ERIDw9ntbHA3MK8kO8a4Mim7U2Q2GdDXy22HZiLF5devet6G');
INSERT INTO customer (name, surname, email, password) values ('osman','karaagacli','test2@test.com','$2a$10$HqCx8ERIDw9ntbHA3MK8kO8a4Mim7U2Q2GdDXy22HZiLF5devet6G');
INSERT INTO customer (name, surname, email, password) values ('test','testsurname','test3@test.com','$2a$10$HqCx8ERIDw9ntbHA3MK8kO8a4Mim7U2Q2GdDXy22HZiLF5devet6G');

-- Order
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (1,1,1,2,22,'2022-01-19 10:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (2,2,2,1,5, '2022-01-21 19:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (3,3,3,1,10,'2022-01-23 18:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (4,1,1,3,35,'2022-02-11 16:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (5,1,3,3,35,'2022-02-19 15:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (6,3,2,3,35,'2022-03-19 14:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (7,3,1,3,35,'2022-03-22 13:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (8,4,3,3,35,'2022-03-15 12:36:32.95701');
insert into "order" (book_id,customer_id,status_id,piece,price,order_date) values (9,2,2,3,35,'2022-04-19 11:33:32.95701');
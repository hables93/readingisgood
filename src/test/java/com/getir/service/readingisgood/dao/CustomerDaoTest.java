package com.getir.service.readingisgood.dao;


import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.util.DaoTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@ActiveProfiles(profiles = {"test"})
public class CustomerDaoTest {

    @Autowired
    private DaoTestHelper daoTestHelper;

    @Autowired
    private CustomerDao customerDao;


    @Test
    @Transactional
    @Rollback
    void testInsertCustomer(){
        int customerId = customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        assertTrue(customerId > 0, "Error at inserting customer.");
    }

    @Test
    @Transactional
    @Rollback
    void getCustomer(){
        int customerId = customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        assertNotNull(customerDao.getCustomer(customerId), "Error at get customer");
    }

    @Test
    @Transactional
    @Rollback
    void getCustomerByEmail(){
        customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        assertNotNull(customerDao.getCustomerByEmail("alper@test.com"), "Error at get customer");
    }

    @Test
    @Transactional
    @Rollback
    void getCustomerList(){
        customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        assertNotNull(customerDao.getCustomerList(), "Error at get customer list.");
    }

    @Test
    @Transactional
    @Rollback
    void updateCustomer(){
        int customerId = customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        Customer customer = customerDao.getCustomer(customerId);
        log.info("Customer name : "+customer.getName()+" email :"+customer.getEmail());
        customer.setName("test");
        customer.setSurname("testsurname");
        assertTrue(customerDao.updateCustomer(customer), "Error at update customer.");
        customer = customerDao.getCustomer(customerId);
        log.info("Customer name : "+customer.getName()+" email :"+customer.getEmail());
    }

    @Test
    @Transactional
    @Rollback
    void deleteCustomer(){
        int customerId = customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        assertTrue(customerDao.deleteCustomer(customerId),"Error at delete customer.");
    }

    @Test
    @Transactional
    @Rollback
    void updateCustomerPassword(){
        int customerId = customerDao.insertCustomer(daoTestHelper.createCustomerRequest());
        Customer customer = customerDao.getCustomer(customerId);
        log.info("Customer password : "+customer.getPassword());
        customer.setPassword("ABCDFGH");
        assertTrue(customerDao.updateCustomer(customer), "Error at update customer.");
        customer = customerDao.getCustomer(customerId);
        log.info("Customer password : "+customer.getPassword());
    }
}

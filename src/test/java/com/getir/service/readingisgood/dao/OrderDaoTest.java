package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.util.DaoTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@ActiveProfiles(profiles = {"test"})
public class OrderDaoTest {

    @Autowired
    private DaoTestHelper daoTestHelper;

    @Autowired
    private OrderDao orderDao;


    @Test
    @Transactional
    @Rollback
    void testInsertOrder(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertTrue(orderId > 0, "Error at inserting order.");
    }

    @Test
    @Transactional
    @Rollback
    void getOrder(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertTrue(orderId > 0, "Error at inserting order.");
        assertNotNull(orderDao.getOrder(orderId), "Error at get order.");
    }

    @Test
    @Transactional
    @Rollback
    void getOrderDetail(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertNotNull(orderDao.getOrderDetail(orderId),"Error at get order detail.");
    }

    @Test
    @Transactional
    @Rollback
    void getOrderList(){
        orderDao.insertOrder(daoTestHelper.createOrderRequest());
        orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertNotNull(orderDao.getOrderList(),"Error at get order list.");
    }

    @Test
    @Transactional
    @Rollback
    void updateOrder(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertTrue(orderId > 0, "Error at inserting order.");
        Order order = orderDao.getOrder(orderId);
        log.info("Order's book_id : "+order.getBookId()+" customer_id : "+order.getCustomerId()+" price : "+order.getPrice());
        order.setPrice(300);
        order.setBookId(2);
        order.setCustomerId(2);
        assertTrue(orderDao.updateOrder(order),"Error at update order.");
        order = orderDao.getOrder(orderId);
        log.info("Order's book_id : "+order.getBookId()+" customer_id : "+order.getCustomerId()+" price : "+order.getPrice());
    }

    @Test
    @Transactional
    @Rollback
    void deleteOrder(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertTrue(orderDao.deleteOrder(orderId),"Error at delete order.");
    }

    @Test
    @Transactional
    @Rollback
    void updateOrderStatus(){
        int orderId = orderDao.insertOrder(daoTestHelper.createOrderRequest());
        Order order = orderDao.getOrder(orderId);
        log.info("Order's status : "+order.getStatusId());
        order.setStatusId(2);
        assertTrue(orderDao.updateOrder(order),"Error at update order.");
        order = orderDao.getOrder(orderId);
        log.info("Order's status : "+order.getStatusId());
    }

}

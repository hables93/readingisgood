package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.dao.BookDao;
import com.getir.service.readingisgood.model.Book;
import com.getir.service.readingisgood.util.DaoTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
@ActiveProfiles(profiles = {"test"})
public class BookDaoTest {

    @Autowired
    private DaoTestHelper daoTestHelper;
    @Autowired
    private BookDao bookDao;

    @Test
    @Transactional
    @Rollback
    void testInsertBook(){
        int bookId = bookDao.insertBook(daoTestHelper.createBookRequest());
        assertTrue(bookId > 0, "Error at inserting book.");
    }

    @Test
    @Transactional
    @Rollback
    void testGetBook(){
        int bookId = bookDao.insertBook(daoTestHelper.createBookRequest());
        assertNotNull(bookDao.getBook(bookId), "Error at get book.");
    }

    @Test
    @Transactional
    @Rollback
    public void testGetBookList(){
        bookDao.insertBook(daoTestHelper.createBookRequest());
        bookDao.insertBook(daoTestHelper.createBookRequest());
        List<Book> bookList = bookDao.getBookList();
        assertNotNull(bookList, "Error at get book list.");
    }

    @Test
    @Transactional
    @Rollback
    void testUpdateBook(){
        int bookId = bookDao.insertBook(daoTestHelper.createBookRequest());
        Book book = bookDao.getBook(bookId);
        log.info("book stock : "+book.getStock()+ " price : "+book.getPrice());
        book.setStock(15);
        book.setPrice(100);
        assertTrue(bookDao.updateBook(book),"Error at update book.");
        book = bookDao.getBook(bookId);
        log.info("book stock : "+book.getStock()+ " price : "+book.getPrice());
    }

    @Test
    @Transactional
    @Rollback
    void testDeleteBook(){
        int bookId = bookDao.insertBook(daoTestHelper.createBookRequest());
        Book book = bookDao.getBook(bookId);
        assertTrue(bookDao.deleteBook(bookId),"Error at delete book.");
        assertNull(bookDao.getBook(bookId),"Error delete is not success.");
    }

    @Test
    @Transactional
    @Rollback
    void testUpdateBookStock(){
        int bookId = bookDao.insertBook(daoTestHelper.createBookRequest());
        Book book = bookDao.getBook(bookId);
        log.info("book stock : "+book.getStock());
        assertTrue(bookDao.updateBookStock(1,bookId),"Error at update book stock.");
        book = bookDao.getBook(bookId);
        log.info("book stock : "+book.getStock());
    }
}

package com.getir.service.readingisgood.dao;

import com.getir.service.readingisgood.util.DaoTestHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@Slf4j
@ActiveProfiles(profiles = {"test"})
public class StatisticsDaoTest {

    @Autowired
    private DaoTestHelper daoTestHelper;

    @Autowired
    private StatisticsDao statisticsDao;

    @Autowired
    private OrderDao orderDao;

    @Test
    @Transactional
    @Rollback
    void testGetStatistics(){
        orderDao.insertOrder(daoTestHelper.createOrderRequest());
        assertNotNull(statisticsDao.getStatistics(),"Error at get statistics");
    }

}

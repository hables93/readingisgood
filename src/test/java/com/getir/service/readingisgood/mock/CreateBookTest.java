package com.getir.service.readingisgood.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.dao.BookDao;
import com.getir.service.readingisgood.model.Book;
import com.getir.service.readingisgood.util.MockObjects;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ActiveProfiles( profiles={"local"})
@ContextConfiguration(classes = {
        BookDao.class
})
public class CreateBookTest {

    private static ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private BookDao bookDao;

    @Test
    protected void givenCorrectRequestCreateBookShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Book bookResponse = objectMapper.readValue(MockObjects.bookObject,Book.class);
        Book bookObject = objectMapper.readValue(MockObjects.createBookRequest,Book.class);

        Mockito.doReturn(bookResponse.getId()).when(bookDao).insertBook(bookObject);

        assertFalse(bookResponse.getId() <= 0,
                " Request : "+bookObject +
                        " Response : "+bookResponse
        );
    }

    @Test
    protected void givenCorrectRequestGetBookShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Book bookResponse = objectMapper.readValue(MockObjects.bookObject,Book.class);
        Book bookObject = objectMapper.readValue(MockObjects.createBookRequest,Book.class);

        Mockito.doReturn(bookResponse).when(bookDao).getBook(bookObject.getId());

        assertFalse(bookResponse.getId() <= 0,
                " Request : "+bookObject +
                        " Response : "+bookResponse
        );
    }

    @Test
    protected void givenCorrectRequestUpdateBookShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Book bookResponse = objectMapper.readValue(MockObjects.bookObject,Book.class);
        Book bookObject = objectMapper.readValue(MockObjects.createBookRequest,Book.class);

        Mockito.doReturn(true).when(bookDao).updateBook(bookObject);

        assertFalse(bookResponse.getId() <= 0,
                " Request : "+bookObject +
                        " Response : "+bookResponse
        );
    }
}

package com.getir.service.readingisgood.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.dao.OrderDao;
import com.getir.service.readingisgood.model.Order;
import com.getir.service.readingisgood.util.MockObjects;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ActiveProfiles( profiles={"local"})
@ContextConfiguration(classes = {
        OrderDao.class
})
public class CreateOrderTest {

    private static ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private OrderDao orderDao;

    @Test
    protected void givenCorrectRequestCreateOrderShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Order orderResponse = objectMapper.readValue(MockObjects.customerObject,Order.class);
        Order orderRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Order.class);

        Mockito.doReturn(orderResponse.getId()).when(orderDao).insertOrder(orderRequest);

        assertFalse(orderResponse.getId() <= 0,
                " Request : "+orderRequest +
                        " Response : "+orderResponse
        );
    }

    @Test
    protected void givenCorrectRequestGetOrderShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Order orderResponse = objectMapper.readValue(MockObjects.customerObject,Order.class);
        Order orderRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Order.class);

        Mockito.doReturn(orderResponse).when(orderDao).getOrder(orderRequest.getId());

        assertFalse(orderResponse.getId() <= 0,
                " Request : "+orderRequest +
                        " Response : "+orderResponse
        );
    }
}

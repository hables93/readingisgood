package com.getir.service.readingisgood.mock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getir.service.readingisgood.dao.CustomerDao;
import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.util.MockObjects;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@ActiveProfiles( profiles={"local"})
@ContextConfiguration(classes = {
        CustomerDao.class
})
public class CustomerMockTest {

    private static ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private CustomerDao customerDao;

    @Test
    protected void givenCorrectRequestCreateCustomerShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Customer customerResponse = objectMapper.readValue(MockObjects.customerObject,Customer.class);
        Customer customerRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Customer.class);

        Mockito.doReturn(customerResponse.getId()).when(customerDao).insertCustomer(customerRequest);
        assertFalse(customerResponse.getId() <= 0,
                " Request : "+customerRequest +
                        " Response : "+customerResponse
        );
    }

    @Test
    protected void givenCorrectRequestGetCustomerShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Customer customerResponse = objectMapper.readValue(MockObjects.customerObject,Customer.class);
        Customer customerRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Customer.class);

        Mockito.doReturn(customerResponse).when(customerDao).getCustomer(customerRequest.getId());

        assertFalse(customerResponse.getId() <= 0,
                " Request : "+customerRequest +
                        " Response : "+customerResponse
        );


    }

    @Test
    protected void givenCorrectRequestUpdateCustomerShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Customer customerResponse = objectMapper.readValue(MockObjects.customerObject,Customer.class);
        Customer customerRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Customer.class);

        customerRequest.setSurname("test");
        Mockito.doReturn(true).when(customerDao).updateCustomer(customerRequest);

        assertFalse(customerResponse.getSurname().equals("test"),
                " Request : "+customerRequest +
                        " Response : "+customerResponse
        );
    }

    @Test
    protected void givenCorrectRequestDeleteCustomerShouldReturnNotThrow() throws JsonProcessingException {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Customer customerResponse = objectMapper.readValue(MockObjects.customerObject,Customer.class);
        Customer customerRequest = objectMapper.readValue(MockObjects.createCustomerRequest,Customer.class);

        Mockito.doReturn(true).when(customerDao).updateCustomer(customerRequest);
    }
}

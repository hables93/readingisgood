package com.getir.service.readingisgood.util;

public class MockObjects {

    public static String createBookRequest = "{\n" +
            "    \"name\" : \"testbook\",\n" +
            "    \"stock\" : 30,\n" +
            "    \"price\" : 100.5\n" +
            "}";

    public static String bookObject = "{\n" +
            "    \"id\": 10,\n" +
            "    \"name\": \"testbook\",\n" +
            "    \"stock\": 30,\n" +
            "    \"price\": 100.5\n" +
            "}";

    public static String createCustomerRequest = "{\n" +
            "    \"name\" : \"sru\",\n" +
            "    \"surname\" : \"alp\",\n" +
            "    \"email\" : \"test13@test.com\",\n" +
            "    \"password\" : \"123456\"\n" +
            "}";

    public static String customerObject = "{\n" +
            "    \"id\": 4,\n" +
            "    \"name\": \"sru\",\n" +
            "    \"surname\": \"alp\",\n" +
            "    \"email\": \"test13@test.com\",\n" +
            "    \"password\": \"123456\"\n" +
            "}";

    public static String createOrderRequest  = "{\n" +
            "    \"bookId\": 1,\n" +
            "    \"customerId\": 1,\n" +
            "    \"piece\": 1,\n" +
            "    \"price\": 10\n" +
            "}";

    public static String orderObject = "{\n" +
            "    \"id\": 10,\n" +
            "    \"customerId\": 1,\n" +
            "    \"bookId\": 1,\n" +
            "    \"piece\": 0,\n" +
            "    \"price\": 1.0,\n" +
            "    \"statusId\": 1,\n" +
            "    \"orderDate\": 1642669128510\n" +
            "}";

}

package com.getir.service.readingisgood.util;

import com.getir.service.readingisgood.model.Book;
import com.getir.service.readingisgood.model.Customer;
import com.getir.service.readingisgood.model.Order;
import org.springframework.stereotype.Component;

@Component
public class DaoTestHelper {

    public Book createBookRequest(){
        return Book.builder()
                .name("book1")
                .price(10)
                .stock(5)
                .build();
    }

    public Customer createCustomerRequest(){
        return Customer.builder()
                .name("alper")
                .surname("karaagaacli")
                .password("123456")
                .email("alper@test.com")
                .build();
    }

    public Order createOrderRequest(){
        return Order.builder()
                .bookId(1)
                .customerId(1)
                .piece(1)
                .price(10)
                .build();
    }

}
